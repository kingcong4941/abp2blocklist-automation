# abp2blocklist Automation Script

## Attribution

The scripts in this repository are based on the original `sitescripts` script, 
located [here](https://gitlab.com/eyeo/devoperations/legacy/sitescripts/tree/master/sitescripts/content_blocker_lists)

## Introduction

The automation scripts in this repo are used to convert ABP filter lists to content blocker lists. 
Conversion happens by using [abp2blocklist](https://gitlab.com/eyeo/adblockplus/abp2blocklist)

## Requirements

- [Python v3.6+](https://www.python.org)

## Running the scripts

You have two options to run the scripts: 
1. If you install this module using `pip` (or any other package manager of your
choice) - **recommended** - a console script named `fl2blocking_list` 
will become available in the virtual environment and can be used to run the scripts.
The signature of the script looks as follows:
```bash
fl2blocking_list [-h] [-v] [-d D] [--abp2blocklist ABP2BLOCKLIST] {txt,batch} ...
```

The `-d` option can be used to specify a local directory where the script will
look for the filter lists.

2. If choose not to install the module, you can simply run it as 
`python -m abp2blocklist_automation.convert` to achieve the same behaviour.

In the examples below, I will use the shorter version of the command. There are 
2 main entry points that can be used with that script 

### Single output

Usage:
```bash
fl2blocking_list txt [-h] [--remote REMOTE] input [input ...] output expires   
```
When in this mode, the script can take any number of filterlists as input, 
converting and merging them into a single content blocking list. Here, the 
`--remote` option can be used to specify the fallback url used by the script if
any of the specified filter lists cannot be found locally. 

### Batch conversion

Usage:
```bash
fl2blocking_list batch [-h] [-o OUTPUT] operations
```
In this mode, the script can take an entire batch of single conversion jobs. 

Each of these jobs is specified using a special operations file. This file is 
expected to be a json with the following format:

```
[
  ...
  {
    "input": [
        "filterlist 1",
        ...
        "filterlist n",
        ...
    ],
    "output": <Path to the file where the resulting blocking list will be saved.>,
    "expires": <Number the days until the content blocking list will expire.>,
    "endpoint": <Fallback remote endpoint used if the filter lists cannot be found locally.>,
  }
  ...
]

```

For a working example of an operations file, see `operations.json` in the repository.