# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2020-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from . import metadata_txt as converter
from . import loading


def get_operations(local_dir, operations_path, out_dir):
    """Get all operations from the operations file.

    Parameters
    ----------
    local_dir: str
        The local directory where the script should look for the filterlist.
    operations_path: str
        Path to the `.json` file where the operations can be extracted from.
    out_dir: str
        The directory where the resulting content blocking lists should be
        saved.

    """
    with open(operations_path) as operations_stream:
        operations_list = json.load(operations_stream)

    for item in operations_list:
        yield {
            'filter_lists': [
                loading.get_filter_list_contents(
                    fl, local_dir,
                    item.get('endpoint', loading.DEFAULT_FALLBACK_ENDPOINT),
                )
                for fl in item['input']
            ],
            'output': os.path.join(out_dir, item['output']),
            'expires': item['expires'],
        }


def main(args):
    """Convert a batch of filter lists in txt format.

    Parameters
    ----------
    args: argparse.Namespace
        With the parsed arguments.

    """
    operations = get_operations(
        args.d, args.operations, args.output,
    )
    for item in operations:
        item['abp2blocklist_repo'] = args.abp2blocklist
        item['skip_metadata'] = args.skip_metadata
        item['compress'] = args.compress
        converter.convert(**item)
