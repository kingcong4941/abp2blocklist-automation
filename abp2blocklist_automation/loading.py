# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2020-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.


__all__ = ['get_filter_list_contents']


from contextlib import closing
import urllib.request as request
import re
import os


DEFAULT_FALLBACK_ENDPOINT = 'https://easylist-downloads.adblockplus.org'
_FL_VERSION_REGEX = r'^(?:[^[!])|^!\s*Version:\s*(.+)$'


def get_filter_list_contents(filter_list, local_dir=None, fallback_url=None):
    """Get the contents of a filterlist.

    The function will look for the requested filterlist in the following
    places, in order of priority:
        1. Locally, in `local_dir`
        2. Remote, using `fallback_url`
        3. Remote, using the default URL: easylist-downloads.adblockplus.org

    Parameters
    ----------
    filter_list: str
        The name of the filter list that is requested.
    local_dir: str
        The local directory where this function should look for the filter
        list.
    fallback_url: str
        The URL where the script should look if the filterlist could not be
        found locally.

    Returns
    -------
    dict
        With the filterlist contents and metadata. It will have the
        following structure:

        {
            `body`: <The actual body of the filterlist>,
            `metadata`: {
                `version`: <The version number, as extracted from the
                            filterlist header>,
                `url`: <The remote URL where the filterlist can be found>
            }

        }

    """
    if local_dir is None and fallback_url is None:
        # None is provided, so we'll go with the default
        return _download_filter_list(
            request.urljoin(DEFAULT_FALLBACK_ENDPOINT, filter_list),
        )

    if fallback_url is None:
        # Use default.
        remote_url = request.urljoin(DEFAULT_FALLBACK_ENDPOINT, filter_list)
    else:
        # Use the provided fallback.
        remote_url = request.urljoin(fallback_url, filter_list)

    if local_dir is None:
        # Fallback url is provided, but no local directory. So going with that.
        return _download_filter_list(remote_url)

    file_path = os.path.join(local_dir, filter_list)
    if os.path.isfile(file_path):
        return _read_filter_list(file_path, remote_url)

    return _download_filter_list(remote_url)


def _read_filter_list(path, remote_url):
    with open(path) as in_stream:
        body = in_stream.read()

    version = re.search(_FL_VERSION_REGEX, body, re.MULTILINE).group(1)

    return {
        'body': body,
        'metadata': {'version': version, 'url': remote_url},
    }


def _download_filter_list(url):
    with closing(request.urlopen(url)) as response:
        body = response.read().decode()

    version = re.search(_FL_VERSION_REGEX, body, re.MULTILINE).group(1)

    return {
        'body': body,
        'metadata': {'version': version, 'url': url},
    }
