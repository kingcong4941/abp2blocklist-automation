# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2020-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.


__all__ = ['convert']


import logging
import subprocess
from collections import OrderedDict
import json
import threading
import time
import os
import zipfile

from . import loading


def pipe_stream_to_process(process, lines):
    """Pass a stream of data to a process' standard input.

    Parameters
    ---------
    process: subprocess.Popen
        The process to pass the data to.
    lines: list of bytes
        The data to be passed to the process.

    """
    try:
        for line in lines:
            process.stdin.write(line)
        logging.info('Sent filter list data to node process.')
    finally:
        process.stdin.close()


def convert(filter_lists,
            output,
            expires,
            abp2blocklist_repo,
            skip_metadata,
            compress):
    """Convert and merge a set of filterlists to a JSON format.

    Parameters
    ----------
    filter_lists: list of dict
        With the contents of the filter lists to be converted.
    output: str
        Path to the file where the resulting JSON will be saved.
    expires: str
        Number of days for which the created JSON will be valid.
    abp2blocklist_repo: str
        Path to the local `ab2blocklist` repository, that will be used to
        convert those filterlists to a content blocking list format.
    metadata: bool
        Whether or not to include metadata during the conversion.
    compress: bool
        Whether or not to compress the output to ZIP.

    """
    if expires == '1':
        expires = '1 day'
    else:
        expires = '{} days'.format(expires)

    logging.info('Processing {} filter lists...'.format(len(filter_lists)))

    # Running the node-based converter.
    input_lines = [fl['body'].encode() for fl in filter_lists]
    process = subprocess.Popen(('node', 'abp2blocklist.js'),
                               stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                               cwd=abp2blocklist_repo)

    threading.Thread(
        target=pipe_stream_to_process, args=(process, input_lines)
    ).run()

    block_lists = OrderedDict((
        ('version', time.strftime('%Y%m%d%H%M', time.gmtime())),
        ('expires', expires),
        ('sources', [fl['metadata'] for fl in filter_lists])
    ))

    if skip_metadata:
        converted_rules = json.loads(process.stdout.read().decode())
    else:
        block_lists['rules'] = json.loads(process.stdout.read().decode())

    return_code = process.wait()
    if return_code != 0:
        raise Exception('abp2blocklist_automation failed with exit code: '
                        '{}'.format(return_code))

    logging.info('Converted filter list and added metadata.')

    with open(output, 'w') as out_stream:
        if skip_metadata:
            json.dump(converted_rules,
                      out_stream,
                      indent=2,
                      separators=(',', ':'))
        else:
            json.dump(block_lists, out_stream, indent=2, separators=(',', ':'))

    if compress:
        compress_to_zip(output)


def compress_to_zip(file):
    old_cwd = os.getcwd()
    try:
        dirname = os.path.dirname(file)
        filename = os.path.split(file)[1]
        output = '{}.zip'.format(
            os.path.splitext(filename)[0]
        )
        os.chdir(dirname)
        with zipfile.ZipFile(output,
                             'w',
                             compression=zipfile.ZIP_DEFLATED) as zipped:
            zipped.write(filename)
            zipped.close()
    finally:
        os.chdir(old_cwd)


def main(args):
    """Convert & merge a set of filter lists to content blocking lists.

    Parameters
    ----------
    args: argparse.Namespace
        The parsed arguments for this script.

    """
    filter_lists = [
        loading.get_filter_list_contents(
            fl, args.d, args.remote,
        ) for fl in args.input
    ]
    convert(filter_lists,
            args.output,
            args.expires,
            args.abp2blocklist,
            args.skip_metadata,
            args.compress)
