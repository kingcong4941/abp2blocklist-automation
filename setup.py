# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2020-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.


from setuptools import setup


DESCRIPTION = ('Convert, merge and format filter lists and content blocking '
               'lists.')
LONG_DESCR = DESCRIPTION

setup(
    name='abp2blocklist_automation',
    version='0.0.1',
    description=DESCRIPTION,
    long_description=LONG_DESCR,
    url='https://gitlab.com/eyeo/sandbox//',
    author='eyeo GmbH',
    author_email='info@eyeo.com',
    license='GPLv3',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Filter list engineers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3',
    ],
    keywords='adblockplus filterlist blocklist',
    packages=['abp2blocklist_automation'],
    entry_points={
        'console_scripts': [
            'fl2blocking_list=abp2blocklist_automation.convert:main',
        ],
    },
)
