# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2020-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import os
import io

import pytest
import py

DATA_PATH = py.path.local(__file__).dirpath('data')


@pytest.fixture
def data_dir():
    return DATA_PATH


@pytest.fixture
def filters_dir():
    return DATA_PATH.join('filter_lists')


@pytest.fixture
def out_dir(tmpdir):
    path = tmpdir.join('out')
    os.mkdir(str(path))
    return path


@pytest.fixture
def operations_file(data_dir):
    return data_dir.join('operations.json')


@pytest.fixture
def filterlist(data_dir):
    return data_dir.join('filterlist.txt')


class TestingInput(io.StringIO):

    def write(self, content):
        assert content.decode() == 'foo'


class MockPOpen:

    class MockStream:

        def read(self):
            return b'text'

    def __init__(self, *args, **kwargs):
        pass

    @property
    def stdin(self):
        return TestingInput()

    def wait(self):
        return 0

    @property
    def stdout(self):
        return self.MockStream()


class MockPOpenError(MockPOpen):

    def wait(self):
        return 1


def mockdownloader(url):
    return {
        'body': 'foo',
        'metadata': {
            'version': '10',
            'url': url,
        },
    }


def mock_json_loads(*args, **kwargs):
    return {
        'foo': 'bar', 'baz': 'bam'
    }
