# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2020-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import abp2blocklist_automation.batch as batch
import abp2blocklist_automation.loading as loading
from .conftest import mockdownloader


def test_get_operations(operations_file, out_dir, filters_dir, monkeypatch):
    monkeypatch.setattr(loading, '_download_filter_list', mockdownloader)
    expected_out = [
        {
            'filter_lists': [{
                'body': 'foo',
                'metadata': {
                    'url': 'https://easylist-downloads.adblockplus.org/foo',
                    'version': '10',
                },
            }],
            'output': str(out_dir.join('bar.json')),
            'expires': 3,
        },
        {
            'filter_lists': [
                {'body': 'foo',
                 'metadata': {'url': 'https://foo.com/1', 'version': '10'},
                 },
                {'body': 'foo',
                 'metadata': {'url': 'https://foo.com/2', 'version': '10'},
                 },
                {'body': 'foo',
                 'metadata': {'url': 'https://foo.com/3', 'version': '10'},
                 },
            ],
            'output': str(out_dir.join('baz.json')),
            'expires': 2,
        }
    ]
    result = list(batch.get_operations(
        str(filters_dir), str(operations_file), str(out_dir),
    ))

    assert result[1] == expected_out[1]
