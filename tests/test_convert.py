# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2020-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

from abp2blocklist_automation import convert
from abp2blocklist_automation import metadata_txt


def get_mock_main_txt(filter_lists):
    def mock_main_txt():
        args = convert.parse_args()

        assert args.runnable == metadata_txt.main
        assert args.abp2blocklist == '/foo/abp2blocklist'
        assert args.output == 'foo.json'
        assert args.input == filter_lists
        assert args.expires == '10'

    return mock_main_txt


def test_args_single_input(script_runner, monkeypatch):
    monkeypatch.setattr(convert, 'main', get_mock_main_txt(['foo.txt']))

    ret = script_runner.run(
        'fl2blocking_list', '--abp2blocklist', '/foo/abp2blocklist', 'txt',
        'foo.txt', 'foo.json', '10',
    )

    assert ret.success


def test_args_no_abp2blocklist(script_runner):
    exp_msg = 'No path to the content blocking list converter found!'
    ret = script_runner.run(
        'fl2blocking_list', 'txt', 'foo.txt', 'foo.json', '10',
    )

    assert not ret.success
    assert exp_msg in ret.stderr


def test_args_multiple_input_single_output(script_runner, monkeypatch):
    monkeypatch.setattr(convert, 'main',
                        get_mock_main_txt(['foo.txt', 'www.bar.com']))

    ret = script_runner.run(
        'fl2blocking_list', '--abp2blocklist', '/foo/abp2blocklist', 'txt',
        'foo.txt', 'www.bar.com', 'foo.json', '10',
    )

    assert ret.success
