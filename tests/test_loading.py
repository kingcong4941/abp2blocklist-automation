# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2020-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from abp2blocklist_automation import loading
from .conftest import mockdownloader


@pytest.mark.parametrize('local_dir,url,exp_url', [
    ('str(filters_dir)', 'https://website.com', 'https://website.com/foo'),
    ('None', 'https://foo.com', 'https://foo.com/foo'),
    ('None', None, 'https://easylist-downloads.adblockplus.org/foo'),
])
def test_loading_from_web(filters_dir, local_dir, url, exp_url, monkeypatch):
    monkeypatch.setattr(loading, '_download_filter_list', mockdownloader)
    local_dir = eval(local_dir)
    result = loading.get_filter_list_contents('foo', local_dir, url)

    assert result['metadata']['url'] == exp_url


def test_loading_local(filters_dir):
    remote_url = 'test.com/filterlist.txt'
    result = loading.get_filter_list_contents(
        'filterlist.txt', str(filters_dir), remote_url,
    )

    assert result['metadata']['url'] == remote_url
    assert result['metadata']['version'] == '6'
